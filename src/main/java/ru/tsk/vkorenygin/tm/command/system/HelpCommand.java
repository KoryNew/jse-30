package ru.tsk.vkorenygin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-h";
    }

    @Override
    public @NotNull String name() {
        return "help";
    }

    @Override
    public @Nullable String description() {
        return "display list of possible actions";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            String result = "";
            if (!DataUtil.isEmpty(command.name())) result += command.name() + " ";
            if (!DataUtil.isEmpty(command.arg())) result += "(" + command.arg() + ") ";
            if (!DataUtil.isEmpty(command.description())) result += "- " + command.description();
            System.out.println(result);
        }
    }

}
