package ru.tsk.vkorenygin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.util.NumberUtil;

public class InfoCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-i";
    }

    @Override
    public @NotNull String name() {
        return "info";
    }

    @Override
    public @Nullable String description() {
        return "display system information";
    }

    @Override
    public void execute() {
        System.out.println("- INFO -");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat =
                (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

}
