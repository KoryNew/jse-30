package ru.tsk.vkorenygin.tm.exception.user;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied.");
    }

}
